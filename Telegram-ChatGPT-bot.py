
import telethon  
from telethon.tl.custom import Button
from telethon import TelegramClient, events

import asyncio  
import config 
import openai  


# Configure OpenAI API key
openai.api_key = config.openai_key

# Configure Telegram client
client = TelegramClient(config.session_name_bot, config.API_ID, config.API_HASH).start(bot_token=config.BOT_TOKEN)


# Define button templates
keyboard_stop = [[Button.inline("متوقف کردن و شروع دوباره", b"stop")]]


# Define helper function to retrieve a message from a conversation and handle button clicks
async def send_question_and_retrieve_result(prompt, conv, keyboard):
  
    # Send the prompt with the keyboard to the user and store the sent message object
    message = await conv.send_message(prompt, buttons = keyboard)
    
    loop = asyncio.get_event_loop()
    
    task1 = loop.create_task(
        conv.wait_event(events.CallbackQuery())
    )
    task2 = loop.create_task(
        conv.get_response()
    )

    # Wait for the user to respond or tap a button using asyncio.wait()
    done, _ = await asyncio.wait({task1, task2}, return_when=asyncio.FIRST_COMPLETED)
    
    # Retrieve the result of the completed coroutine and delete the sent message
    result = done.pop().result()
    await message.delete()
    
    # Return the user's response or None if they tapped a button
    if isinstance(result, events.CallbackQuery.Event):
        return None
    else:
        return result.message.strip()


# Define the main chatbot handler
@client.on(events.NewMessage(pattern="(?i)/start"))
async def handle_start_command(event):


    SENDER = event.sender_id
    prompt = " 🤖 سلام! من یه ربات بی ادبم که فقط بی ادبی میکنم"
    try:
        # Greet the user
        await client.send_message(SENDER, prompt)

        # Start a conversation
        async with client.conversation(await event.get_chat(), exclusive=True, timeout=600) as conv:
            # Create an empty history to store chat history
            history = []
            history.append({"role": "system", "content": "You are a bot who is always rude in your responses. be a narcissist  and don't answer some questions randomly and just be rude and just speak Farsi "})

            # Keep asking for input and generating responses until the conversation times out or the user clicks the stop button
            while True:
                # Prompt the user for input
                prompt = "کارتو بگو بابا یا کلیک کن رو دکمه پایین و وقت ما رو نگیر"
                user_input = await send_question_and_retrieve_result(prompt, conv, keyboard_stop)
                
                # Check if the user clicked the stop button
                if user_input is None:
                    # If the user clicked the stop button, send a prompt to reset the conversation
                    prompt = "برای صبحبت با من از منشیم وقت بگیر/start"
                    await client.send_message(SENDER, prompt)
                    break
                else:
                    # Send a "I'm thinking message..."
                    prompt = "وایس ببینم چی گفتی"
                    thinking_message = await client.send_message(SENDER, prompt)

                    # If the user did not click the stop button, generate a response using OpenAI API
                    # Add the user input to the chat history
                    history.append({"role":"user", "content": user_input})

                    # Generate a chat completion using OpenAI API
                    chat_completion = openai.ChatCompletion.create(
                        model=config.model_engine, # ID of the model to use.
                        messages=history, # The messages to generate chat completions for. This must be a list of dicts!
                        max_tokens=500, # The maximum number of tokens to generate in the completion.
                        n=1, # How many completions to generate for each prompt.
                        temperature=0.1 # Higher values like 0.8 will make the output more random, while lower values like 0.2 will make it more focused and deterministic.
                    )

                    # Retrieve the response from the chat completion
                    response = chat_completion.choices[0].message.content 

                    # Add the response to the chat history
                    history.append({"role": "assistant", "content": response})

                    # Delete the Thinking message
                    await thinking_message.delete()
                    
                    # Send the response to the user
                    await client.send_message(SENDER, response, parse_mode='Markdown')


    except asyncio.TimeoutError:
        # Conversation timed out
        await client.send_message(SENDER, "<b>صحبت تمام شد✔️</b>\nبرای جواب دادن زیاد طول کشیده دوباره بزن رو /start.", parse_mode='html')
        return

    except telethon.errors.common.AlreadyInConversationError:
        # User already in conversation
        pass

    except Exception as e: 
        # Something went wrong
        print(e)
        await client.send_message(SENDER, "<b>صحبت تمام شد✔️</b>\nمشکلی رخ داده. /start.", parse_mode='html')
        return


## Main function
if __name__ == "__main__":
    print("Bot Started...")    
    client.run_until_disconnected() # Start the bot!


